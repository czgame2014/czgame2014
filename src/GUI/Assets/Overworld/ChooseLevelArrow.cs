using System;
using UnityEngine;
using System;

public class ChooseLevelArrow
{

	public const float V = 0.1f;

	public Vector2 pos, target;
	public GameObject modell;

	public ChooseLevelArrow(Vector2 p, GameObject m)
	{
		modell = m;
		setPos (p);
		setTarget (p);
	}

	public void setTarget(Vector2 t){
		target = t;
	}

	public void setPos(Vector2 p){
		pos = p;
		modell.transform.position = pos;
	}

	public void move(Vector2 d){
		setPos(pos + d);
	}

	public void moveToTarget(){
		Vector2 d = target - pos;
		if (d.sqrMagnitude <= V * V) {
			setPos(target);
			return;
		}
		d.Normalize ();
		setPos (pos + d * V);
	
	}
}

