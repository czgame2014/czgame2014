﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.IO;

[XmlInclude(typeof(ChooseLevelNode))]
public class ChooseLevelScript : MonoBehaviour {

	int currentlvl = 0;
	ChooseLevelArrow arrow;

	public ChooseLevelNode[] graph_level;

	// Use this for initialization
	void Start(){

		XmlSerializer deserializer = new XmlSerializer(typeof(ChooseLevelNode[]));
		StreamReader textReader = new StreamReader("res/levelchooser/graph.xml");
		graph_level = (ChooseLevelNode[])deserializer.Deserialize(textReader);
		textReader.Close();
		GameObject objarrow = GameObject.Find ("Arrow");

		foreach(ChooseLevelNode cln in graph_level){
			cln.pos.y += objarrow.renderer.bounds.size.y/2;
		}

		arrow = new ChooseLevelArrow (graph_level[currentlvl].pos, objarrow);

		print(arrow.pos);
		print (arrow.modell.transform.position);
	}

	public void changeLevel(int level){
		currentlvl=level;

		arrow.setTarget (graph_level[level].pos);
		
		print("Neuer Level" + currentlvl);
	}

	void checkArrowKeys(){
		int arrownumber = -1;
		if(Input.GetKeyDown(KeyCode.UpArrow)) {
			arrownumber=ChooseLevelNode.UP;
			print("Pfeilnachoben");
		}
		if(Input.GetKeyDown(KeyCode.RightArrow)) {
			arrownumber=ChooseLevelNode.RIGHT;
			print("Pfeilnachrechts");
		}
		if(Input.GetKeyDown(KeyCode.DownArrow)) {
			arrownumber=ChooseLevelNode.DOWN;
			print("Pfeilnachunten");
		}
		if(Input.GetKeyDown(KeyCode.LeftArrow)) {
			arrownumber=ChooseLevelNode.LEFT;
			print("Pfeilnachlinks");
		}
		if(arrownumber!=-1){//Es wurde eine Pfeiltaste gedrückt
			changeLevel(graph_level[currentlvl].adjList[arrownumber]);
			
			print("Neuer Level" + currentlvl);
		}
	}

	void action(){
		arrow.moveToTarget ();
	}

	// Update is called once per frame
	void Update () {
		checkArrowKeys ();

		action ();
	}
}
